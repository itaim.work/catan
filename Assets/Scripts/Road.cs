using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour
{
    [SerializeField] private bool IsEmpty, hasChecked;
    [SerializeField] private int type;
    [SerializeField] private List<Point> points;
    [SerializeField] private GameManager GameManagerScript;

    // Start is called before the first frame update
    void Start()
    {
        IsEmpty = true;
        points = new List<Point>();
        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void SetColor(Color color)
    {
        GetComponent<Renderer>().material.color = color;
    }
    public void SetIsEmpty(bool value)
    {
        this.IsEmpty = value;
    }
    public bool GetIsEmpty()
    {
        return this.IsEmpty;
    }
    public void SetType(int value)
    {
        this.type = value;
    }
    public int Get_Type()
    {
        return this.type;
    }
    public void SetMeshRender(bool value)
    {
        GetComponent<Renderer>().enabled = value;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Point"))
        {
            if (!points.Contains(collision.transform.GetComponent<Point>()))
            {
                points.Add(collision.transform.GetComponent<Point>());
            }
        }
    }
    public void SetHasChecked(bool value)
    {
        this.hasChecked = value;
    }
    public bool GetHasChecked()
    {
        return hasChecked;
    }
    public Point GetOppositePoint(Point point)
    {
        Point pointToReturn = null;
        for (int i = 0; i < points.Count; i++)
        {
            if (point != points[i])
            {
                pointToReturn = points[i];
            }
        }
        return pointToReturn;
    }

    private void OnMouseDown()
    {
        if (gameObject.GetComponent<MeshRenderer>().enabled)
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
            this.type = GameManagerScript.GetActivePlayer();
            IsEmpty = false;
            hasChecked = false;
            GameManagerScript.ClearIndicator();
            GameManagerScript.GetPlayers()[type].SetRoadCount(GameManagerScript.GetPlayers()[type].GetRoadCount() + 1);
            SetColor(GameManagerScript.GetPlayers()[type].GetPlayerColor());
            if (GameManagerScript.GetIsStart())
            {
                GameManagerScript.EndTurn();
            }
            else
            {
                GameManagerScript.GetPlayers()[GameManagerScript.GetActivePlayer()].SetResource("Wood", -1);
                GameManagerScript.GetPlayers()[GameManagerScript.GetActivePlayer()].SetResource("Stone", -1);
                GameManagerScript.UpdateButton();
                GameManagerScript.CheckCanConvert();
            }
        }
    }
}
