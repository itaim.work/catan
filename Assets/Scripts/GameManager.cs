using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Text TurnDisplayText;
    [SerializeField] private Text CubeResultDisplayText;
    [SerializeField] private GameObject[] PointsTempArray;
    private Point[,] Points2DArray;
    [SerializeField] private bool canUpgrade, IsStart = true, CanRaid = false;
    [SerializeField] private GameObject BuildMenu;

    [SerializeField] private GameObject BuildMenuButton;

    [SerializeField] private GameObject ShuffleButton;
    [SerializeField] private GameObject StartBuildMenu;
    [SerializeField] private GameObject StartRoadButton;
    [SerializeField] private GameObject StartVillageButton;
    [SerializeField] private GameObject VillageButton;
    [SerializeField] private GameObject RoadButton;
    [SerializeField] private GameObject TownButton;
    [SerializeField] private GameObject PlayerDisplay;
    [SerializeField] private GameObject EndTurnButton;
    [SerializeField] private GameObject RollButton;
    [SerializeField] private GameObject ConvertMenu;
    [SerializeField] private GameObject ConvertButton;
    [SerializeField] private GameObject OpenConvertMenuButton;
    [SerializeField] private GameObject CloseConvertNenu;
    [SerializeField] private GameObject RaidDisplay;
    [SerializeField] private GameObject EndMenu;
    [SerializeField] private int ActivePlayer, StartBuildCounter = 0;
    [SerializeField] private List<PlayerController> Players;
    [SerializeField] private NodesController NodesControllerScript;


    // Start is called before the first frame update
    void Start()
    {
        IsStart = true;
        int x, y, lastDigits;
        Points2DArray = new Point[12, 11];

        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 11; j++)
            {
                Points2DArray[i, j] = null;
            }
        }

        PointsTempArray = GameObject.FindGameObjectsWithTag("Point");

        for (int i = 0; i < PointsTempArray.Length; i++)
        {
            string temp = PointsTempArray[i].transform.name;

            if (temp.Length > 8)
            {
                lastDigits = Int32.Parse(temp.Substring(6, 3));

                if (lastDigits > 200)
                {
                    x = lastDigits / 100;
                    y = lastDigits - (x * 100);
                }
                else
                {
                    x = lastDigits / 10;
                    y = lastDigits % 10;
                }

            }
            else
            {
                lastDigits = Int32.Parse(temp.Substring(6, 2));
                x = lastDigits / 10;
                y = lastDigits % 10;
            }

            Points2DArray[x, y] = PointsTempArray[i].GetComponent<Point>();
            Points2DArray[x, y].SetIndex(x, y);
        }



    }

    // Update is called once per frame
    void Update()
    {
        TurnDisplayText.text = "Turn: " + Players[ActivePlayer].GetPlayerName() + ".";

        if (Input.GetKeyDown(KeyCode.Space))
        {
            BuildViilageStart();
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            Roll();
        }

    }

    /// <summary>
    /// the function rolls the cubes. 
    /// </summary>
    public void Roll()
    {
        int CubeResult = 0;
        CubeResult += UnityEngine.Random.Range(1, 7); // draw random number between 1-6
        CubeResult += UnityEngine.Random.Range(1, 7); // ""

        CubeResultDisplayText.text = "Result: " + CubeResult; // set the result text to the new result
        if (CubeResult == 7) // check raid status
        {
            CanRaid = true;
        }
        else
        {
            ClaimResource(CubeResult);
        }
        RollButton.SetActive(false);
        if (CanRaid)
        {
            SetBuildMenu(false);
            SetRaidDisplay(true);
        }
        else
        {
            SetEndTurnButton(true);
            UpdateButton();
            CheckCanConvert();
        }

    }
    public void ClaimResource(int CubeResult)
    {
        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 11; j++)
            {
                if (Points2DArray[i, j] != null && !Points2DArray[i, j].GetIsEmpty())
                {
                    Node tempNode = Points2DArray[i, j].HasNode(CubeResult);
                    if (tempNode != null)
                    {
                        if (Points2DArray[i, j].GetIsTown())
                        {
                            Players[Points2DArray[i, j].GetOwner_ID()].SetResource(tempNode.GetNodeName(), 2);
                        }
                        else
                        {
                            Players[Points2DArray[i, j].GetOwner_ID()].SetResource(tempNode.GetNodeName(), 1);

                        }
                    }
                }
            }
        }

    }
    public void OpenBuildMenu()
    {
        ShuffleButton.SetActive(false);
        BuildMenuButton.SetActive(false);
        StartBuildMenu.SetActive(true);
    }
    public void CloseMenuButton()
    {
        BuildMenuButton.SetActive(true);
        BuildMenu.SetActive(false);

    }
    public void ShuffleMap()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void BuildViilageStart()
    {
        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 11; j++)
            {
                if (Points2DArray[i, j] != null)
                {
                    if (Points2DArray[i, j].GetCanBuild())
                    {
                        Points2DArray[i, j].GetIndicator().SetActive(true);
                    }
                    else if (Points2DArray[i, j].GetIsEmpty())
                    {
                        Points2DArray[i, j].GetIndicator().SetActive(false);
                    }
                }
            }
        }
    }
    public void ClearIndicator()
    {
        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 11; j++)
            {
                if (Points2DArray[i, j] != null)
                {
                    if (Points2DArray[i, j].GetIsEmpty())
                    {
                        Points2DArray[i, j].GetIndicator().SetActive(false);
                    }
                    for (int k = 0; k < Points2DArray[i, j].GetRoads().Count; k++)
                    {
                        if (Points2DArray[i, j].GetRoads()[k].GetIsEmpty())
                        {
                            Points2DArray[i, j].GetRoads()[k].SetMeshRender(false);
                        }
                        Points2DArray[i, j].GetRoads()[k].SetHasChecked(false);
                    }
                }
            }
        }

    }
    public void DisableBuildOnNear(GameObject OriginPoint)
    {
        int x, y, lastDigits;

        if (OriginPoint.name.Length > 8)
        {
            lastDigits = Int32.Parse(OriginPoint.name.Substring(6, 3));

            if (lastDigits > 200)
            {
                x = lastDigits / 100;
                y = lastDigits - (x * 100);
            }
            else
            {
                x = lastDigits / 10;
                y = lastDigits % 10;
            }

        }
        else
        {
            lastDigits = Int32.Parse(OriginPoint.name.Substring(6, 2));
            x = lastDigits / 10;
            y = lastDigits % 10;
        }

        if (x == 0)
        {
            Points2DArray[x, y].GetComponent<Point>().SetCanBuild(false);
            Points2DArray[x + 1, y - 1].GetComponent<Point>().SetCanBuild(false);
            Points2DArray[x + 1, y + 1].GetComponent<Point>().SetCanBuild(false);
        }
        else if (x == 11)
        {
            Points2DArray[x, y].GetComponent<Point>().SetCanBuild(false);
            Points2DArray[x - 1, y + 1].GetComponent<Point>().SetCanBuild(false);
            Points2DArray[x - 1, y - 1].GetComponent<Point>().SetCanBuild(false);
        }
        else if (x == 1 && y == 2 || x == 3 && y == 1 || x == 5 && y == 0)
        {
            Points2DArray[x, y].GetComponent<Point>().SetCanBuild(false);
            Points2DArray[x - 1, y + 1].GetComponent<Point>().SetCanBuild(false);
            Points2DArray[x + 1, y].GetComponent<Point>().SetCanBuild(false);

        }
        else if (x == 1 && y == 8 || x == 3 && y == 9 || x == 5 && y == 10)
        {
            Points2DArray[x, y].GetComponent<Point>().SetCanBuild(false);
            Points2DArray[x - 1, y - 1].GetComponent<Point>().SetCanBuild(false);
            Points2DArray[x + 1, y].GetComponent<Point>().SetCanBuild(false);
        }
        else if (x == 6 && y == 0 || x == 8 && y == 1 || x == 10 && y == 2)
        {
            Points2DArray[x, y].GetComponent<Point>().SetCanBuild(false);
            Points2DArray[x - 1, y].GetComponent<Point>().SetCanBuild(false);
            Points2DArray[x + 1, y + 1].GetComponent<Point>().SetCanBuild(false);
        }
        else if (x == 6 && y == 10 || x == 8 && y == 9 || x == 10 && y == 8)
        {
            Points2DArray[x, y].SetCanBuild(false);
            Points2DArray[x - 1, y].SetCanBuild(false);
            Points2DArray[x + 1, y - 1].SetCanBuild(false);

        }
        else if (x % 2 != 0)
        {
            Points2DArray[x, y].SetCanBuild(false);
            Points2DArray[x + 1, y].SetCanBuild(false);
            Points2DArray[x - 1, y + 1].SetCanBuild(false);
            Points2DArray[x - 1, y - 1].SetCanBuild(false);
        }
        else
        {
            Points2DArray[x, y].SetCanBuild(false);
            Points2DArray[x - 1, y].SetCanBuild(false);
            Points2DArray[x + 1, y + 1].SetCanBuild(false);
            Points2DArray[x + 1, y - 1].SetCanBuild(false);

        }



    }
    public void BuildRoad()
    {

        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 11; j++)
            {
                if (Points2DArray[i, j] != null && Points2DArray[i, j].GetOwner_ID() == ActivePlayer)
                {
                    TurnOnRoads(Points2DArray[i, j]);

                }
            }
        }
    }
    public void TurnOnRoads(Point point)
    {

        for (int k = 0; k < point.GetRoads().Count; k++)
        {
            if (point.GetRoads()[k].Get_Type() == -1 && !point.GetRoads()[k].GetHasChecked())
            {
                point.GetRoads()[k].SetMeshRender(true);
                point.GetRoads()[k].SetHasChecked(true);

            }
            else if (point.GetRoads()[k].Get_Type() == ActivePlayer && !point.GetRoads()[k].GetHasChecked())
            {
                point.GetRoads()[k].SetHasChecked(true);
                TurnOnRoads(point.GetRoads()[k].GetOppositePoint(point));
            }
        }

    }
    public void BuildVillage()
    {
        bool flag = false;


        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 11; j++)
            {
                flag = false;
                if (Points2DArray[i, j] != null && Points2DArray[i, j].GetCanBuild())
                {
                    for (int k = 0; k < Points2DArray[i, j].GetRoads().Count; k++)
                    {
                        if (Points2DArray[i, j].GetRoads()[k].Get_Type() == ActivePlayer && !Points2DArray[i, j].GetRoads()[k].GetIsEmpty())
                        {
                            flag = true;
                        }
                    }
                    if (flag)
                    {
                        Points2DArray[i, j].GetIndicator().SetActive(true);
                    }
                }
            }
        }


    }
    public int GetActivePlayer()
    {
        return this.ActivePlayer;
    }
    public void UpgradeVillage()
    {
        canUpgrade = true;
        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 11; j++)
            {
                if (Points2DArray[i, j] != null && Points2DArray[i, j].GetOwner_ID() == ActivePlayer && !Points2DArray[i, j].GetIsEmpty() && !Points2DArray[i, j].GetIsTown())
                {
                    Points2DArray[i, j].BuildCastle();
                }
            }
        }

    }
    public void ClearAfterUpgrade()
    {
        canUpgrade = false;
        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 11; j++)
            {
                if (Points2DArray[i, j] != null && Points2DArray[i, j].GetOwner_ID() == ActivePlayer && !Points2DArray[i, j].GetIsEmpty() && !Points2DArray[i, j].GetIsTown())
                {
                    Points2DArray[i, j].SetToVillage();
                }
            }
        }

    }
    public bool GetCanUpgrade()
    {
        return this.canUpgrade;
    }
    public void SetCanUpgrade(bool value)
    {
        this.canUpgrade = value;
    }
    public List<PlayerController> GetPlayers()
    {
        return this.Players;
    }
    public bool GetIsStart()
    {
        return this.IsStart;
    }
    public void EndTurn()
    {
        if (IsStart)
        {
            bool flag = false, RoadFlag = false;
            for (int k = 0; k < Players.Count; k++)
            {
                if (Players[k].GetVillageCount() < 2)
                {
                    flag = true;
                }
                if (Players[k].GetRoadCount() < 2)
                {
                    RoadFlag = true;
                }
            }
            if (!flag)
            {
                StartVillageButton.SetActive(false);
                StartRoadButton.SetActive(true);
            }
            if (!RoadFlag)
            {
                StartRoadButton.SetActive(false);
                BuildMenu.SetActive(true);
                IsStart = false;
                PlayerDisplay.SetActive(true);
                FillPlayerDisplay();
                RollButton.SetActive(true);
                CheckCanConvert();
                UpdateButton();
            }
        }
        else
        {
            RollButton.SetActive(true);
            CheckCanConvert();
        }
        EndTurnButton.SetActive(false);
        ActivePlayer++;
        if (ActivePlayer >= Players.Count)
        {
            ActivePlayer = 0;
        }
        if (!IsStart)
        {
            FillPlayerDisplay();
        }
    }
    public void FillPlayerDisplay()
    {
        int[] TempResourceAmount = Players[ActivePlayer].GetResourceAmount();
        Text TempText;
        TempText = GameObject.Find("WheetDisplay").GetComponent<Text>();
        TempText.text = "Wheet: " + TempResourceAmount[0];
        TempText = GameObject.Find("WoodDisplay").GetComponent<Text>();
        TempText.text = "Wood: " + TempResourceAmount[1];
        TempText = GameObject.Find("WoolDisplay").GetComponent<Text>();
        TempText.text = "Wool: " + TempResourceAmount[2];
        TempText = GameObject.Find("IronDisplay").GetComponent<Text>();
        TempText.text = "Iron: " + TempResourceAmount[3];
        TempText = GameObject.Find("StoneDisplay").GetComponent<Text>();
        TempText.text = "Stone: " + TempResourceAmount[4];
        TempText = GameObject.Find("PointDisplay").GetComponent<Text>();
        TempText.text = "Points: " + Players[ActivePlayer].GetPoints();

    }
    public void UpdateButton()
    {
        int[] TempResourceAmount = Players[ActivePlayer].GetResourceAmount();

        if (TempResourceAmount[1] > 0 && TempResourceAmount[4] > 0)
        {
            RoadButton.GetComponent<Button>().enabled = true;
            RoadButton.GetComponentInChildren<Text>().text = "Build Road \0 1xWood & 1xStone Required";
        }
        else
        {
            RoadButton.GetComponent<Button>().enabled = false;
            RoadButton.GetComponentInChildren<Text>().text = "Missing Resources for Road\n 1xWood & 1xStone Required";
        }
        if (TempResourceAmount[1] > 0 && TempResourceAmount[4] > 0 && TempResourceAmount[2] > 0 && TempResourceAmount[0] > 0)
        {
            VillageButton.GetComponent<Button>().enabled = true;
            VillageButton.GetComponentInChildren<Text>().text = "Build Village\n 1 of each Required (Minus Iron)";
        }
        else
        {
            VillageButton.GetComponent<Button>().enabled = false;
            VillageButton.GetComponentInChildren<Text>().text = "Missing Resources for Button\n 1 of each Required (Minus Iron)";
        }
        if (TempResourceAmount[0] > 1 && TempResourceAmount[3] > 2)
        {
            TownButton.GetComponent<Button>().enabled = true;
            TownButton.GetComponentInChildren<Text>().text = "Build Town \n 2xWheet & 3xIron Required";
        }
        else
        {
            TownButton.GetComponent<Button>().enabled = false;
            TownButton.GetComponentInChildren<Text>().text = "Missing Resources For Town\n 2xWheet & 3xIron Required";
        }


    }
    public void OpenConvertMenu()
    {
        int[] TempResourceAmount = Players[ActivePlayer].GetResourceAmount();
        BuildMenu.SetActive(false);
        ConvertMenu.SetActive(true);
        Dropdown TempDropDown = GameObject.Find("SourceDropDown").GetComponent<Dropdown>();
        TempDropDown.ClearOptions();
        List<string> Options = new List<string>();
        for (int i = 0; i < TempResourceAmount.Length; i++)
        {
            if (TempResourceAmount[i] > 3)
            {
                switch (i)
                {
                    case 0:
                        Options.Add("Wheet");
                        break;
                    case 1:
                        Options.Add("Wood");
                        break;
                    case 2:
                        Options.Add("Wool");
                        break;
                    case 3:
                        Options.Add("Iron");
                        break;
                    case 4:
                        Options.Add("Stone");
                        break;
                }
            }
        }
        if (Options.Count > 0)
        {
            TempDropDown.AddOptions(Options);
        }
    }
    public void CloseConvertMenu()
    {
        BuildMenu.SetActive(true);
        ConvertMenu.SetActive(false);
        CheckCanConvert();
    }
    public void ConvertResource()
    {
        string Source, Dest;
        Dropdown TempDropDown = GameObject.Find("SourceDropDown").GetComponent<Dropdown>();
        Source = TempDropDown.options[TempDropDown.value].text;
        TempDropDown = GameObject.Find("DestDropDown").GetComponent<Dropdown>();
        Dest = TempDropDown.options[TempDropDown.value].text;
        Players[ActivePlayer].SetResource(Source, -4);
        Players[ActivePlayer].SetResource(Dest, 1);
        CloseConvertMenu();
    }
    public void CheckCanConvert()
    {
        int[] TempResourceAmount = Players[ActivePlayer].GetResourceAmount();
        bool CanConvert = false;
        for (int i = 0; i < TempResourceAmount.Length; i++)
        {
            if (TempResourceAmount[i] > 3)
            {
                CanConvert = true;
            }
        }

        if (!CanConvert)
        {
            OpenConvertMenuButton.GetComponent<Button>().enabled = false;
            OpenConvertMenuButton.GetComponentInChildren<Text>().text = "No Resource To Convert";
        }
        else
        {
            OpenConvertMenuButton.GetComponent<Button>().enabled = true;
            OpenConvertMenuButton.GetComponentInChildren<Text>().text = "Open Convert Menu";

        }

    }
    public bool GetCanRaid()
    {
        return this.CanRaid;
    }
    public void SetCanRaid(bool value)
    {
        this.CanRaid = value;
    }
    public void SetEndTurnButton(bool value)
    {
        this.EndTurnButton.SetActive(value);
    }
    public void SetBuildMenu(bool value)
    {
        this.BuildMenu.SetActive(value);
    }
    public void SetRaidDisplay(bool value)
    {
        RaidDisplay.SetActive(value);
    }
    public void EndGame(string playerName)
    {
        Time.timeScale = 0;
        EndMenu.SetActive(true);
        Text tempText = GameObject.Find("Winner_name").GetComponent<Text>();
        tempText.text = "Winner: " + playerName;
    }
    public void Restart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }
    public void MainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
