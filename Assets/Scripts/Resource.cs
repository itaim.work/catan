using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource : MonoBehaviour
{
    private string ResourceName;
    private int Counter;

    public Resource(string resourceName, int counter)
    {
        ResourceName = resourceName;
        Counter = counter;
    }

    public string getName()
    {
        return name;
    }

    public int getCounter()
    {
        return Counter;
    }

    public void SetCounter(int value)
    {
        Counter += value;
    }


}
