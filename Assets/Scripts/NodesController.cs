using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodesController : MonoBehaviour
{
    [SerializeField] private Mesh[] Meshs;

    [SerializeField] private GameObject[] Nodes;
    private int[] res = new int[5];

    // Start is called before the first frame update
    void Start()
    {

        int ptr = 0;
        res[0] = 4; // wheet hex9
        res[1] = 4; // wood hex 13
        res[2] = 4; // wool hex 10
        res[3] = 3; // iron hex 23
        res[4] = 3; // Stone hex 32

        Nodes = GameObject.FindGameObjectsWithTag("Node");

        for (int i = 0; i < Nodes.Length; i++)
        {
            ptr = Random.Range(0, 5);
            if (res[ptr] > 0)
            {
                res[ptr]--;
                switch (ptr)
                {
                    case 0:
                        Nodes[i].GetComponent<Node>().setNode("Wheet", Meshs[ptr]);
                        break;
                    case 1:
                        Nodes[i].GetComponent<Node>().setNode("Wood", Meshs[ptr]);
                        break;
                    case 2:
                        Nodes[i].GetComponent<Node>().setNode("Wool", Meshs[ptr]);
                        break;
                    case 3:
                        Nodes[i].GetComponent<Node>().setNode("Iron", Meshs[ptr]);
                        break;
                    case 4:
                        Nodes[i].GetComponent<Node>().setNode("Stone", Meshs[ptr]);
                        break;

                }
            }
            else
            {
                i--;
            }
        }
        ResetRaid();

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void ResetRaid()
    {
        for (int i = 0; i < Nodes.Length; i++)
        {
            Nodes[i].GetComponent<Node>().SetIsRaided(false);
        }
    }
}
